<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

class GetProductsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'string',
            'price' => 'numeric',
            'status' => 'boolean',
            'max_price' => 'numeric',
            'min_price' => 'numeric',
            'sort_by' => 'should_in_sorts',
            'sort_order' => 'string|should_in_orders',
            'columns' => 'array|should_in_columns',
            'per_page' => 'numeric|between:5,50',
            'page' => 'numeric'
        ];
    }

    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->getMessageBag();
        throw new HttpResponseException(response()->json([
            'errors' => $errors,
            'message' => 'Query Params Are Not Correct!'
        ], 422));
    }
}
