<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Services;


use App\Models\Product;
use App\Services\Contracts\TwilioServiceContract;
use Twilio\Rest\Client;

class TwilioService implements TwilioServiceContract
{


    /**
     * @param Product $product
     * @return bool
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public function sendSMS(Product $product): bool
    {
        $sid = env('TWILIO_SID');
        $token = env('TWILIO_TOKEN');

        $client = new Client($sid, $token);

        try {
            $client->messages->create(
                '+15005550010',
                [
                    'from' => '+15005550006',
                    'body' => $product->name . ' named product is created successfully!'
                ]
            );

            return true;

        } catch (\Exception $e) {
            return false;
        }


    }
}
