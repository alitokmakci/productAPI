<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Services;


use App\Models\Product;
use App\QueryFilters\Name;
use App\QueryFilters\Price;
use App\QueryFilters\SortBy;
use App\QueryFilters\Status;
use App\Services\Contracts\ProductServiceContract;
use Illuminate\Pipeline\Pipeline;

class ProductService implements ProductServiceContract
{

    /**
     * @param array|null $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(array $filters = null): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $filters = collect($filters);

        $query = Product::query();

        $perPage = 20;

        $page = 1;

        $columns = ['*'];

        if ($filters->has('columns')) {
            $columns = $filters->get('columns');
        }

        if ($filters->has('per_page')) {
            $perPage = $filters->get('per_page');
        }

        if ($filters->has('page')) {
            $page = $filters->get('page');
        }

        if ($filters->isEmpty()) {
            return $query->paginate($perPage, $columns, 'page', $page);
        }

        $products = app(Pipeline::class)
            ->send($query)
            ->through([
                Name::class,
                Price::class,
                Status::class,
                SortBy::class
            ])->thenReturn();

        return $products->paginate($perPage, $columns, 'page', $page);

    }

    /**
     * @param array $data
     * @return Product
     */
    public function create(array $data): Product
    {
        return Product::create($data);
    }

    /**
     * @param Product $product
     * @param array $data
     * @return Product
     */
    public function update(Product $product, array $data): Product
    {
        $product->update($data);

        return $product->fresh();
    }

    /**
     * @param Product $product
     * @return boolean
     */
    public function delete(Product $product): bool
    {
        $product->delete();

        return true;
    }
}
