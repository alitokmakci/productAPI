<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Services\Contracts;


use App\Models\Product;

interface ProductServiceContract
{
    /**
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function index(array $filters): \Illuminate\Contracts\Pagination\LengthAwarePaginator;

    /**
     * @param array $data
     * @return Product
     */
    public function create(array $data): Product;

    /**
     * @param Product $product
     * @param array $data
     */
    public function update(Product $product, array $data);

    /**
     * @param Product $product
     * @return boolean
     */
    public function delete(Product $product): bool;
}
