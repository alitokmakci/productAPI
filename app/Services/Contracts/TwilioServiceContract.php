<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\Services\Contracts;


use App\Models\Product;

interface TwilioServiceContract
{
    public function sendSMS(Product $product);
}
