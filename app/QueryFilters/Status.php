<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\QueryFilters;

use Closure;

class Status extends Filter
{
    public function __construct()
    {
        parent::__construct('status');
    }

    protected function applyFilter($builder)
    {
        return $builder->where('status', request('status'));
    }
}
