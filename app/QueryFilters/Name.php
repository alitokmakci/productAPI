<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\QueryFilters;

use Closure;

class Name extends Filter
{
    public function __construct()
    {
        parent::__construct('name');
    }

    protected function applyFilter($builder)
    {
        return $builder->where('name', 'like', '%' . request('name') . '%');
    }
}
