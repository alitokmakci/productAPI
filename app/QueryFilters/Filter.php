<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\QueryFilters;


use Closure;

abstract class Filter
{
    protected $filter;

    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    public function handle($request, Closure $next)
    {
        $builder = $next($request);

        if (!request()->has($this->filter)) {
            return $builder;
        }

        return $this->applyFilter($builder);
    }

    protected abstract function applyFilter($builder);
}
