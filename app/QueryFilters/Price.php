<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\QueryFilters;

use Closure;

class Price extends Filter
{
    public function __construct()
    {
        parent::__construct('price');
    }

    protected function applyFilter($builder)
    {
        return $builder->where('price', request('price'));
    }
}
