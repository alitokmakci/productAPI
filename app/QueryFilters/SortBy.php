<?php
/**
 * @author Ali TOKMAKCI <alitokmakci@outlook.com>
 */

namespace App\QueryFilters;

use Closure;

class SortBy extends Filter
{
    public function __construct()
    {
        parent::__construct('sort_by');
    }

    protected function applyFilter($builder)
    {
        $order = 'asc';

        if (request()->has('sort_order')) {
            $order = request('sort_order');
        }

        if (is_array(request('sort_by'))) {
            foreach (request('sort_by') as $sort) {
                $builder->orderBy($sort, $order);
            }

            return $builder;
        }

        if (is_string(request('sort_by'))) {
            return $builder->orderBy(request('sort_by'), $order);
        }


        return $builder->where('stats', request('sort_by'));
    }
}
