<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    private const COLUMNS = ['id', 'name', 'price', 'status', 'created_at', 'updated_at'];

    private const ORDERS = ['asc', 'desc', 'ascending', 'descending'];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('should_in_sorts', function($attribute, $value, $parameters)
        {
            if (is_array($value)) {
                foreach ($value as $item) {
                    if (!in_array($item, $this::COLUMNS)) {
                        return false;
                    }
                }
            }

            if (is_string($value) && !in_array($value, $this::COLUMNS)) {
                return false;
            }

            return true;

        });

        Validator::extend('should_in_orders', function($attribute, $value, $parameters)
        {
            if (!in_array($value, $this::ORDERS)) {
                return false;
            }

            return true;

        });

        Validator::extend('should_in_columns', function($attribute, $value, $parameters)
        {
            foreach ($value as $item) {
                if (!in_array($item, $this::COLUMNS)) {
                    return false;
                }
            }

            return true;

        });
    }
}
