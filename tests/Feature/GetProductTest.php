<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetProductTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_product_response()
    {
        Product::factory(20)->create();

        $response = $this->get('/api/products/');

        $products = Product::paginate(20);

        $response->assertStatus(200);

        $responseArray = json_decode($response->getContent(), true);

        $i = 0;
        foreach ($products->items() as $product) {
           $this->assertEquals($product->toArray(), $responseArray['data'][$i]);
           $i++;
        }
    }
}
