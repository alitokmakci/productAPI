<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateProductTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_update_product()
    {
        $product = Product::factory()->create();

        $response = $this->put('/api/products/' . $product->id, [
            'name' => 'Example Product',
            'price' => 9.99,
            'status' => false,
        ]);

        $response->assertStatus(202);

        $response->assertJson([
            'name' => 'Example Product',
            'price' => 9.99,
            'status' => false,
        ]);
    }
}
