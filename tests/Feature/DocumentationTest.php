<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DocumentationTest extends TestCase
{
    /**
     * Documentation url testing
     *
     * @return void
     */
    public function test_get_api_documentation_page()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
}
