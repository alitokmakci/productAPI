<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Services\TwilioService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    use RefreshDatabase;

    protected $example = [
        'name' => 'Example Product',
        'price' => 9.99,
        'status' => false,
    ];

    /**
     * Create Product Test
     *
     * @return void
     */
    public function test_creating_product()
    {
        $response = $this->postJson('/api/products', $this->example);

        $response->assertStatus(201)
            ->assertJson($this->example);
    }

    /**
     * @throws \Twilio\Exceptions\ConfigurationException
     */
    public function test_sending_sms_after_creating_product()
    {
        $product = new Product($this->example);

        $product->saveQuietly();

        $sms = (new \App\Services\TwilioService)->sendSMS($product);

        $this->assertTrue($sms);
    }
}
