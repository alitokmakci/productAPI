<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
    <title>Product API!</title>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="#">Product API</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Documentation</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <div class="row mt-5">
        <div class="col-md-3">
            <ul class="nav flex-column position-fixed">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#index">
                        Get The Products
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#show" tabindex="-1" aria-disabled="true">
                        Get A Single Product
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#create">
                        Create New Product
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#update">
                        Update An Existing Product
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#delete" tabindex="-1" aria-disabled="true">
                        Delete Product
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-md-8 offset-1">
            <div class="mb-5" id="index">
                <h4>
                    Get The Products
                    <span class="badge bg-success">GET</span>
                </h4>
                <code>/api/products</code>
                <p class="mt-2">You can use this method to get list of products or search for products.</p>
                <h5 class="my-4">Request Parameters:</h5>
                <p>There is no request parameter for this method.</p>
                <h5 class="my-4">Query Parameters:</h5>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        name
                        <span class="badge bg-primary">string</span>
                    </h6>
                    <p class="fw-light">The name of the products.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        price
                        <span class="badge bg-primary">decimal</span>
                    </h6>
                    <p class="fw-light">The products with a given price.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        status
                        <span class="badge bg-primary">boolean</span>
                    </h6>
                    <p class="fw-light">Status of products.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        sort_by
                        <span class="badge bg-primary">string | array</span>
                    </h6>
                    <p>
                        Valid Values:
                        <span class="badge bg-secondary">id</span>
                        <span class="badge bg-secondary">name</span>
                        <span class="badge bg-secondary">status</span>
                        <span class="badge bg-secondary">price</span>
                        <span class="badge bg-secondary">created_at</span>
                        <span class="badge bg-secondary">updated_at</span>
                    </p>
                    <p>Default: <span class="badge bg-secondary">['id']</span></p>
                    <p class="fw-light">Can be used for sort the product list.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        sort_order
                        <span class="badge bg-primary">string</span>
                    </h6>
                    <p>
                        Valid Values:
                        <span class="badge bg-secondary">ascending</span>
                        <span class="badge bg-secondary">descending</span>
                        <span class="badge bg-secondary">asc</span>
                        <span class="badge bg-secondary">desc</span>
                    </p>
                    <p>Default: <span class="badge bg-secondary">ascending</span></p>
                    <p class="fw-light">Order way of the products.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        columns
                        <span class="badge bg-primary">array</span>
                    </h6>
                    <p>
                        Valid Values:
                        <span class="badge bg-secondary">id</span>
                        <span class="badge bg-secondary">name</span>
                        <span class="badge bg-secondary">price</span>
                        <span class="badge bg-secondary">status</span>
                        <span class="badge bg-secondary">created_at</span>
                        <span class="badge bg-secondary">updated_at</span>
                    </p>
                    <p>Default: <span class="badge bg-secondary">['id', 'name', 'price', 'status', 'created_at', 'updated_at']</span></p>
                    <p class="fw-light">Required columns for your app.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        page
                        <span class="badge bg-primary">integer</span>
                    </h6>
                    <p>Default: <span class="badge bg-secondary">1</span></p>
                    <p class="fw-light"></p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        per_page
                        <span class="badge bg-primary">integer</span>
                    </h6>
                    <p>
                        Valid Values:
                        <span class="badge bg-secondary">integer between 5 - 50</span>
                    </p>
                    <p>Default: <span class="badge bg-secondary">20</span></p>
                    <p class="fw-light">Products per request.</p>
                </div>

                <div class="example mb-5">
                    <h5>Example Response:</h5>
                    <code>/api/products?per_page=5</code>
                    <pre class="prettyprint mt-3">
{
  "current_page": 1,
  "data": [
    {
      "id": 1,
      "name": "quis enim itaque",
      "price": "13.12",
      "status": true,
      "created_at": "2021-07-09T16:57:20.000000Z",
      "updated_at": "2021-07-09T16:57:20.000000Z"
    },
    {
      "id": 2,
      "name": "ut aut quia",
      "price": "14.07",
      "status": true,
      "created_at": "2021-07-09T16:57:20.000000Z",
      "updated_at": "2021-07-09T16:57:20.000000Z"
    },
    {
      "id": 3,
      "name": "aspernatur similique iure",
      "price": "19.97",
      "status": true,
      "created_at": "2021-07-09T16:57:20.000000Z",
      "updated_at": "2021-07-09T16:57:20.000000Z"
    },
    {
      "id": 4,
      "name": "quidem nemo vero",
      "price": "15.81",
      "status": false,
      "created_at": "2021-07-09T16:57:20.000000Z",
      "updated_at": "2021-07-09T16:57:20.000000Z"
    },
    {
      "id": 5,
      "name": "et et ducimus",
      "price": "15.57",
      "status": true,
      "created_at": "2021-07-09T16:57:20.000000Z",
      "updated_at": "2021-07-09T16:57:20.000000Z"
    }
  ],
  "first_page_url": "http://localhost:8000/api/products?page=1",
  "from": 1,
  "last_page": 40,
  "last_page_url": "http://localhost:8000/api/products?page=40",
  "links": [
    {
      "url": null,
      "label": "&laquo; Previous",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=1",
      "label": "1",
      "active": true
    },
    {
      "url": "http://localhost:8000/api/products?page=2",
      "label": "2",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=3",
      "label": "3",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=4",
      "label": "4",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=5",
      "label": "5",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=6",
      "label": "6",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=7",
      "label": "7",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=8",
      "label": "8",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=9",
      "label": "9",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=10",
      "label": "10",
      "active": false
    },
    {
      "url": null,
      "label": "...",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=39",
      "label": "39",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=40",
      "label": "40",
      "active": false
    },
    {
      "url": "http://localhost:8000/api/products?page=2",
      "label": "Next &raquo;",
      "active": false
    }
  ],
  "next_page_url": "http://localhost:8000/api/products?page=2",
  "path": "http://localhost:8000/api/products",
  "per_page": "5",
  "prev_page_url": null,
  "to": 5,
  "total": 200
}
                </pre>
                </div>

            </div>
            <div class="mb-5" id="show">
                <h4>
                    Get A Single Product
                    <span class="badge bg-success">GET</span>
                </h4>
                <code>/api/products/{id}</code>
                <p class="mt-2">You can use this method to get a specific product.</p>

                <h5 class="my-4">Request Parameters:</h5>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        id
                        <span class="badge bg-primary">integer</span>
                        <span class="badge bg-danger">required</span>
                    </h6>
                    <p class="fw-light">The id of the product.</p>
                </div>

                <h5 class="my-4">Query Parameters:</h5>

                <p>There is no query parameter for this method.</p>

                <div class="example mb-5">
                    <h5>Example Response:</h5>
                    <code>/api/products/1</code>
                    <pre class="prettyprint mt-3">
{
  "id": 1,
  "name": "quis enim itaque",
  "price": "13.12",
  "status": true,
  "created_at": "2021-07-09T16:57:20.000000Z",
  "updated_at": "2021-07-09T16:57:20.000000Z"
}
                </pre>
                </div>
            </div>

            <div class="mb-5" id="create">
                <h4>
                    Create A New Product
                    <span class="badge bg-info">POST</span>
                </h4>
                <code>/api/products</code>
                <p class="mt-2">You can use this method to create a product.</p>

                <h5 class="my-4">Request Parameters:</h5>

                <p>There is no request parameter for this method.</p>

                <h5 class="my-4">Query Parameters:</h5>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        name
                        <span class="badge bg-primary">string</span>
                        <span class="badge bg-danger">required</span>
                    </h6>
                    <p class="fw-light">The name of the product.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        price
                        <span class="badge bg-primary">decimal</span>
                        <span class="badge bg-danger">required</span>
                    </h6>
                    <p class="fw-light">The price of the product.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        status
                        <span class="badge bg-primary">boolean</span>
                        <span class="badge bg-danger">required</span>
                    </h6>
                    <p class="fw-light">The status of the product.</p>
                </div>

                <div class="example mb-5">
                    <h5>Example Response:</h5>
                    <code>/api/products</code>
                    <pre class="prettyprint mt-3">
{
  "id": 1,
  "name": "quis enim itaque",
  "price": "13.12",
  "status": true,
  "created_at": "2021-07-09T16:57:20.000000Z",
  "updated_at": "2021-07-09T16:57:20.000000Z"
}
                </pre>
                </div>
            </div>

            <div class="mb-5" id="update">
                <h4>
                    Update An Existing Product
                    <span class="badge bg-warning">PUT/PATCH</span>
                </h4>
                <code>/api/products/{id}</code>
                <p class="mt-2">You can use this method to update a product.</p>

                <h5 class="my-4">Request Parameters:</h5>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        id
                        <span class="badge bg-primary">integer</span>
                        <span class="badge bg-danger">required</span>
                    </h6>
                    <p class="fw-light">The id of the product.</p>
                </div>

                <h5 class="my-4">Query Parameters:</h5>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        name
                        <span class="badge bg-primary">string</span>
                    </h6>
                    <p class="fw-light">The name of the product.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        price
                        <span class="badge bg-primary">decimal</span>
                    </h6>
                    <p class="fw-light">The price of the product.</p>
                </div>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        status
                        <span class="badge bg-primary">boolean</span>
                    </h6>
                    <p class="fw-light">The status of the product.</p>
                </div>

                <div class="example mb-5">
                    <h5>Example Response:</h5>
                    <code>/api/products/1</code>
                    <pre class="prettyprint mt-3">
{
  "id": 1,
  "name": "quis enim itaque",
  "price": "13.12",
  "status": true,
  "created_at": "2021-07-09T16:57:20.000000Z",
  "updated_at": "2021-07-09T16:57:20.000000Z"
}
                </pre>
                </div>
            </div>

            <div class="mb-5" id="delete">
                <h4>
                    Delete Product
                    <span class="badge bg-danger">DELETE</span>
                </h4>
                <code>/api/products/{id}</code>
                <p class="mt-2">You can use this method to delete a product.</p>

                <h5 class="my-4">Request Parameters:</h5>

                <div class="parameter mb-3 border-bottom">
                    <h6>
                        id
                        <span class="badge bg-primary">integer</span>
                        <span class="badge bg-danger">required</span>
                    </h6>
                    <p class="fw-light">The id of the product.</p>
                </div>

                <h5 class="my-4">Query Parameters:</h5>

                <p>There is no query parameter for this method.</p>

                <div class="example mb-5">
                    <h5>Example Response:</h5>
                    <code>/api/products/1</code>
                    <pre class="prettyprint mt-3">
{
  "deleted": true
}
                </pre>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
