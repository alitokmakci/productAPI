## Project Installation

Clone Project:

```git clone https://gitlab.com/alitokmakci/productAPI.git```

Go to project root directory:

```cd productAPI```

Install composer dependencies:

```composer install```

Copy .env file:

```cp .env-example .env```

Generate Secret Key:

```php artisan key:generate```

*Make sure you edit the <b>.env</b> file for database and Twilio account

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=product_api
DB_USERNAME=root
DB_PASSWORD=

TWILIO_SID=
TWILIO_TOKEN=
```


Run migrations and feed database with dummy data:

```php artisan migrate --seed```

Serve Project on your localhost, port:8000.

```php artisan serve```

and visit http://localhost:8000 address to see API documentation.


## Testing Application

For Testing run command below:

```php artisan test```
